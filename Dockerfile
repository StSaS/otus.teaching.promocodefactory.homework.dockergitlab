FROM mcr.microsoft.com/dotnet/core/sdk:3.1

WORKDIR /app

ENV ASPNETCORE_URLS=http://*:5001
ENV ConnectionStrings:PromoCodeFactoryDb=Host=192.168.99.100;Port=5433;Database=promocode_factory_db;Username=postgres;Password=admin

EXPOSE 5001

COPY src/ /src/

RUN dotnet restore "/src/Otus.Teaching.PromoCodeFactory.sln"

RUN dotnet publish "/src/Otus.Teaching.PromoCodeFactory.WebHost/Otus.Teaching.PromoCodeFactory.WebHost.csproj" -c Release -o /app

CMD dotnet Otus.Teaching.PromoCodeFactory.WebHost.dll